import home from '../pages/HomePage'
import login from '../pages/LoginPage'

describe('Login Page', () => {

    beforeEach(function () {
        home.go()
        cy.fixture('TestInformation.json').then((information) => {
            this.information = information
        })
    })

    it.skip('Navegando até a página de login', function () {
        
        login.clickToEnterLoginPage()
        login.directedToLoginPage()
    })

    it.skip('Criando conta para o perfil de pessoas candidatas', function () {
    
        login.createAccount()
        login.fillOutRegistrationForm(this.information.candidatos)
        login.acceptanceOfTermsAndConditions()
        login.signUp()
        login.profileInformation()
        login.nextButton()
        login.profileInformation2()
        login.nextButton2()
        login.scoreCard()
        login.nextButton3()
    })

    it.skip('Validação do formulário na etapa de preenchimento do perfil', function () {

    
        login.createAccount()
        login.fillOutRegistrationForm(this.information.candidatos2)
        login.acceptanceOfTermsAndConditions()
        login.signUp()
        login.profileInformationValidation()
    })
})