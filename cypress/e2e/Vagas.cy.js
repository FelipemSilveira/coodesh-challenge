import vagas from '../pages/VagasPage'
import home from '../pages/HomePage'

describe('Página de vagas', () => {

    beforeEach(function () {
        home.go()
        cy.fixture('TestInformation.json').then((information) => {
            this.information = information
        })
    })

    it('Buscando por uma empresa que tenha um ou mais resultados de vagas', function () {
        
        vagas.clickOnSeeVacancies()
        vagas.searchingVacancies(this.information.vagas)
        vagas.search()
        vagas.validatingVacancies(this.information.vagas)
    })
})