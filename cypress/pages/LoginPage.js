class LoginPage {
    
    clickToEnterLoginPage() {
        cy.get('a[href="/auth/signin/candidates"]').click()
    }

    directedToLoginPage() {
        cy.get('h2[class="h3 text-primary font-weight-medium mb-0"]')
        .should('have.text', 'Faça o login')   
    }

    createAccount() {
        cy.get('a[href="/auth/signup/candidates"] button').click()
        
    }

    fillOutRegistrationForm(registration) {
        cy.get('input[id="displayName"]').type(registration.nome) 
        cy.get('input[id="email"]').type(registration.email) 
        cy.get('input[id="password"]').type(registration.senha)
    }

    acceptanceOfTermsAndConditions() {
        cy.get('input[id="privacy.gpdr"]').click({force: true})
    }

    signUp() {
        cy.get('button[type="submit"]').click()
    }

    profileInformation() {
        cy.get('span[class="d-block"]').contains('QA / Testes').click()
        cy.get('div[class="mt-3"]').contains('Até um ano').click()
        cy.get('input[type="tel"]').type('71987655467')
        cy.get('input[id="address.city"]').type('Salvador')
        cy.get('input[id="home-office-integral"]').click()
    }

    nextButton() {
        cy.get('div[id="footer-portal"] button').click()
    }

    profileInformation2() {
        cy.get('.col-lg-12.col-12 .css-13tza3w').click()
        cy.get('div[id="react-select-3-option-1"]').click()
        cy.get('select[id="race"]').select('Pessoa Negra')
        cy.get('select[id="gender"]').select('Homem')
        cy.get('select[id="sexual_orientation"]').select('Heterossexual')
    }

    nextButton2() {
        cy.get('.btn-primary.btn-wide').click()
    }

    scoreCard() {
        cy.get('#footer-portal > div > div > a').click()
    }

    nextButton3() {
        cy.get(':nth-child(1) > td > div > div.rc-slider-step > span:nth-child(2)').click()
        cy.get('.btn.btn-primary').click({force: true})
        cy.get('tr:nth-child(1)  span:nth-child(2)').click()
        cy.get('button[type="submit"]').click({force: true})
        cy.get('a[href="#"]').click({force: true})
    }

    profileInformationValidation() {
        cy.wait(10000)
        cy.get('input[id="displayName"]').clear()
        cy.get('div[class="mt-3"]').contains('Até um ano').click()
        cy.get('input[type="tel"]').type('9999999999')
        cy.get('input[id="address.city"]').click()
        cy.get('.btn-primary.btn-wide').click({force: true})
        cy.get('div[class="invalid-feedback"]').contains('Seu nome completo é obrigatório')
        cy.get('div[class="invalid-feedback"]').contains('Este campo deve ser um telefone válido')
        cy.get('div[class="invalid-feedback"]').contains('Em qual cidade você reside? é obrigatório')
        cy.get('div[class="invalid-feedback"]').contains('Este campo é obrigatório')
    }
}

export default new LoginPage;