class HomePage {
    
    go() {
        cy.visit('/')
        cy.get('button[id="onetrust-accept-btn-handler"]').should('is.visible').click()
    }

    homePageCarregamento() {
        cy.get('h1[class="h1 mb-0 font-weight-semi-bold"]')
        .should('have.text', 'Valide seu potencial para alcançar as melhores oportunidades tech')   
    }

}

export default new HomePage;