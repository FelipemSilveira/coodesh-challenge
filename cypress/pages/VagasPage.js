class VagasPage {
    
    clickOnSeeVacancies() {
        cy.get('a[title="Ver Vagas"]').click()
    }

    searchingVacancies(vacancies) {
        cy.get('input[placeholder="Buscar"]').type(vacancies.empresa)  
    }

    search() {
        cy.get('div[class="align-self-lg-end col-lg-2"] button').click()
    }

    validatingVacancies(vacancies){
      cy.get('#content > div > div:nth-child(3)').contains(vacancies.developer)
      cy.get('#content > div > div:nth-child(3)').contains(vacancies.dba)
      cy.get('#content > div > div:nth-child(3)').contains(vacancies.backend)
    }
}

export default new VagasPage;