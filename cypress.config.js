const { defineConfig } = require("cypress");

module.exports = defineConfig({
  projectId: 'x5z2uw',
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    baseUrl: 'https://beta.coodesh.com/'
  },

  "viewportWidth": 1440,
  "viewportHeight": 900,

});
